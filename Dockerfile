FROM python:3.7-slim

RUN apt-get update && apt-get install -y --no-install-recommends python3-tk ghostscript libglib2.0 libsm6 fonts-lato wkhtmltopdf libpango-1.0-0 libpangoft2-1.0-0

RUN apt-get -y --no-install-recommends install software-properties-common && \
    add-apt-repository "deb https://deb.debian.org/debian experimental main" && \
    apt-get update && \
    apt-get -t experimental install -y --no-install-recommends poppler-utils && \
    apt clean && \
    rm -rf /var/lib/apt && \
    rm -rf /var/lib/dpkg
